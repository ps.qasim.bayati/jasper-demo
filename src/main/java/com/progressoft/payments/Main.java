package com.progressoft.payments;

import com.progressoft.payments.generators.pdf.PaymentAdvicePdfGenerator;
import com.progressoft.payments.generators.pdf.modals.PaymentAdvicePdfDetails;
import com.progressoft.payments.generators.pdf.modals.PdfInvoiceDetails;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        PaymentAdvicePdfDetails paymentAdvicePdfDetails = getPaymentAdvicePdfDetails();

        byte[] export = new PaymentAdvicePdfGenerator().export(paymentAdvicePdfDetails);

        try (FileOutputStream out = new FileOutputStream(UUID.randomUUID().toString().concat(".pdf"))) {
            out.write(export);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static PaymentAdvicePdfDetails getPaymentAdvicePdfDetails() {
        PaymentAdvicePdfDetails paymentAdvicePdfDetails = new PaymentAdvicePdfDetails();
        paymentAdvicePdfDetails.setPaymentAmount("123.00 USD");
        paymentAdvicePdfDetails.setPaymentNumber("123");
        paymentAdvicePdfDetails.setBeneficiaryName("Al-Bayati, Qasim");
        paymentAdvicePdfDetails.setBeneficiaryAddress("AzZarqa And Cut..");
        paymentAdvicePdfDetails.setCorporateName("Progressoft Corporation");
        paymentAdvicePdfDetails.setCorporateAddress("Amman - Um Authayna");
        paymentAdvicePdfDetails.setValueDate("2025-12-25");
        paymentAdvicePdfDetails.setDescription("blah blah blah blah blah....");
        paymentAdvicePdfDetails.setLogo(loadFileInputStream("logo.png"));
        paymentAdvicePdfDetails.setBankLogo(loadFileInputStream("bank_logo.png"));
        paymentAdvicePdfDetails.getInvoices().addAll(
                List.of(getPdfInvoiceDetails("0001", "2021/12/15", "510.00 USD"),
                        getPdfInvoiceDetails("0002", "2021/12/16", "520.00 USD"),
                        getPdfInvoiceDetails("0003", "2021/12/17", "530.00 USD"),
                        getPdfInvoiceDetails("0004", "2021/12/18", "540.00 USD"),
                        getPdfInvoiceDetails("0005", "2021/12/19", "550.00 USD"),
                        getPdfInvoiceDetails("0006", "2021/12/20", "560.00 USD"),
                        getPdfInvoiceDetails("0007", "2021/12/21", "570.00 USD"),
                        getPdfInvoiceDetails("0008", "2021/12/22", "580.00 USD")));
        return paymentAdvicePdfDetails;
    }

    private static PdfInvoiceDetails getPdfInvoiceDetails(String number, String date, String amount) {
        PdfInvoiceDetails invoiceDetails3 = new PdfInvoiceDetails();
        invoiceDetails3.setInvoiceDate(date);
        invoiceDetails3.setInvoiceNumber(number);
        invoiceDetails3.setInvoiceAmount(amount);
        return invoiceDetails3;
    }

    static InputStream loadFileInputStream(String templateFilePath) {
        return Main.class.getClassLoader().getResourceAsStream(templateFilePath);
    }
}
