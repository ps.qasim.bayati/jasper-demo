package com.progressoft.payments.generators.pdf.modals;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PaymentAdvicePdfDetails {

    private String corporateName;
    private String corporateAddress;
    private String beneficiaryName;
    private String beneficiaryAddress;
    private String paymentNumber;
    private String valueDate;
    private InputStream logo;
    private InputStream bankLogo;

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCorporateAddress() {
        return corporateAddress;
    }

    public void setCorporateAddress(String corporateAddress) {
        this.corporateAddress = corporateAddress;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryAddress() {
        return beneficiaryAddress;
    }

    public void setBeneficiaryAddress(String beneficiaryAddress) {
        this.beneficiaryAddress = beneficiaryAddress;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String paymentAmount;
    private String description;


    List<PdfInvoiceDetails> invoices = new ArrayList<>();

    public List<PdfInvoiceDetails> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<PdfInvoiceDetails> invoices) {
        this.invoices = invoices;
    }

    public InputStream getLogo() {
        return logo;
    }

    public void setLogo(InputStream logo) {
        this.logo = logo;
    }

    public InputStream getBankLogo() {
        return bankLogo;
    }

    public void setBankLogo(InputStream bankLogo) {
        this.bankLogo = bankLogo;
    }
}
