package com.progressoft.payments.generators.pdf;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

public abstract class JasperPdfGenerationHelper {

    protected byte[] export(JRBeanCollectionDataSource dataSource,
                            HashMap<String, Object> parameters,
                            String reportFilePath) {
        try {
            JasperPrint jasperPrint = JasperFillManager.fillReport(loadJasperReport(reportFilePath), parameters, dataSource);
            return toPdf(jasperPrint);
        } catch (JRException e) {
            throw new IllegalStateException("cannot load jasper template report", e);
        }
    }

    protected byte[] toPdf(JasperPrint jasperPrint) throws JRException {
        JRPdfExporter jrPdfExporter = new JRPdfExporter();
        SimpleExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            jrPdfExporter.setExporterInput(exporterInput);
            jrPdfExporter.exportReport();
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("cannot export jasper report as pdf", e);
        }
    }

    protected JasperReport loadJasperReport(String templateFilePath) throws JRException {
        URL templateFile = this.getClass().getClassLoader().getResource(templateFilePath);
        return (JasperReport) JRLoader.loadObject(templateFile);
    }


}
