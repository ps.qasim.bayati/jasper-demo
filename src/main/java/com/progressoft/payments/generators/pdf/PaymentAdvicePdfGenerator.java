package com.progressoft.payments.generators.pdf;

import com.progressoft.payments.generators.pdf.modals.PaymentAdvicePdfDetails;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.util.HashMap;

public class PaymentAdvicePdfGenerator extends JasperPdfGenerationHelper {

    public static final String PAYMENT_ADVICE_DATA_SOURCE = "paymentAdviceDataSource";

    public byte[] export(PaymentAdvicePdfDetails paymentAdvicePdfDetails) {
        return export(
                createInvoicesDataSource(paymentAdvicePdfDetails),
                prepareParametersMap(createInvoicesDataSource(paymentAdvicePdfDetails), paymentAdvicePdfDetails),
                "PaymentAdvice.jasper");
    }

    private HashMap<String, Object> prepareParametersMap(JRBeanCollectionDataSource dataSource,
                                                         PaymentAdvicePdfDetails paymentAdvicePdfDetails) {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put(PAYMENT_ADVICE_DATA_SOURCE, dataSource);

        parameters.put("corporateName", paymentAdvicePdfDetails.getCorporateName());
        parameters.put("corporateAddress", paymentAdvicePdfDetails.getCorporateAddress());
        parameters.put("beneficiaryName", paymentAdvicePdfDetails.getBeneficiaryName());
        parameters.put("beneficiaryAddress", paymentAdvicePdfDetails.getBeneficiaryAddress());
        parameters.put("paymentNumber", paymentAdvicePdfDetails.getPaymentNumber());
        parameters.put("valueDate", paymentAdvicePdfDetails.getValueDate());
        parameters.put("paymentAmount", paymentAdvicePdfDetails.getPaymentAmount());
        parameters.put("description", paymentAdvicePdfDetails.getDescription());
        parameters.put("adviceLogo", paymentAdvicePdfDetails.getLogo());
        parameters.put("bankLogo", paymentAdvicePdfDetails.getBankLogo());

        return parameters;
    }

    private JRBeanCollectionDataSource createInvoicesDataSource(PaymentAdvicePdfDetails paymentAdvicePdfDetails) {
        return new JRBeanCollectionDataSource(paymentAdvicePdfDetails.getInvoices());
    }

}